package alexandert;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class RomDecTest {
		AlexanderTInl14RomDec romdec = new AlexanderTInl14RomDec();
	
	@Test
	void test() {
		assertEquals(1234,romdec.getDec("MCCXXXIV"),"asdasd");
	}
	@Test
	void test1() {
		assertEquals(1,romdec.getDec("I"));
	}
	@Test
	void test2() {
		assertEquals(2,romdec.getDec("II"));
	}
	@Test
	void test3() {
		assertEquals(3,romdec.getDec("III"));
	}
	@Test
	void test4() {
		assertEquals(4,romdec.getDec("IV"));
	}
	@Test
	void test5() {
		assertEquals(5,romdec.getDec("V"));
	}
	@Test
	void test6() {
		assertEquals(6,romdec.getDec("VI"));
	}
	@Test
	void test7() {
		assertEquals(7,romdec.getDec("VII"));
	}
	@Test
	void test8() {
		assertEquals(8,romdec.getDec("VIII"));
	}

	@Test
	void test9() {
		assertEquals(9,romdec.getDec("IX"));
	}
	@Test
	void test10() {
		assertEquals(10,romdec.getDec("X"));
	}
	@Test
	void test11() {
		assertEquals(11,romdec.getDec("XI"));
	}
	@Test
	void test13() {
		assertEquals(13,romdec.getDec("XIII"));
	}
	@Test
	void test14() {
		assertEquals(14,romdec.getDec("XIV"));
	}
	@Test
	void test15() {
		assertEquals(15,romdec.getDec("XV"));
	}

}
