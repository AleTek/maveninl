package alexandert;

public enum RomerskSiffra {
	I(1), V(5), X(10), L(50), C(100), D(500), M(1000);

	private int tal;

	RomerskSiffra(int tal) {
		this.tal = tal;
	}

	public int getTal() {
		return tal;
	}


	
	
}
