package alexandert;

public class AlexanderTInl14RomDec {

	public int getDec(String romtal) {
		int längd = romtal.length();
		int summa = 0;
		int i = 0;
		
		while (i < längd) {
			char bokstav = romtal.charAt(i);
			int värde = omvandla(bokstav).getTal();
			i++;
			if (i == längd) {
				summa += värde;
			}

			else {
				char bokstav_efter = romtal.charAt(i);
				int värde_efter = omvandla(bokstav_efter).getTal();
				
				if (värde_efter > värde) {
					summa += (värde_efter - värde);
					i++;
				}
				else {
					summa += värde;
				}
			}
		}
		return summa;
	}
	
	public RomerskSiffra omvandla (char bokstav) {
		switch (bokstav) {
		case 'I': return RomerskSiffra.I;
		case 'V': return RomerskSiffra.V;
		case 'X': return RomerskSiffra.X;
		case 'L': return RomerskSiffra.L;
		case 'C': return RomerskSiffra.C;
		case 'D': return RomerskSiffra.D;
		case 'M': return RomerskSiffra.M;
		}
		return null;
	}
	
}
